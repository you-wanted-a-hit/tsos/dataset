# Dataset

## Table of contents

* Intro
* Extraction Strategy
* Labeling Explained


## Intro 

To train our Insight and Reccomendation models, we needed alot of tracks, with numerical features, and effective labels. This is where the [Spotify API](https://developer.spotify.com/documentation/web-api/) came to our help, alongside a little research into their private API.

## Extraction Strategy

To get the song's details, such as song name and artist, we used the [Track endpoint](https://developer.spotify.com/documentation/web-api/reference/#endpoint-get-track) of the Spotify API. for the numerical values representing the track, we used the [Audio Features](https://developer.spotify.com/documentation/web-api/reference/#endpoint-get-audio-features) endpoint of the API.

But in order to use these endpoints we needed to have the song's Spotify ID. Also, we needed a way of effectively labeling our data.

To account for these difficulties we choose to use the official Spotify playlists. If you are a Spotify user, you might already be aware that Spotify curates playlists based on genre. For example, [Jazz playlists](https://open.spotify.com/genre/jazz-playlists).

Instead of cluttering the code with web-scraping, we researched the playlist pages' requests to the private API, and found a request to get the IDs of the playlists that appear on the page.

Now we used the [Playlist items endpoint](https://developer.spotify.com/documentation/web-api/reference/#endpoint-get-playlists-tracks) to get the songs from each playlist, removed duplicates, and used the previously mentioned Track and Audio Features endpoints to get the full track.

## Labeling Explained

### *Disclaimer: The dataset was not validated for duplicate labeling, meaning for cases when the same song is labeled as multiple genres.*

The labeling strategy is pretty straightforward: if a song appear on a genre-curated playlist, the song is labeled as that genre.
